<img width="660" height="200" src="banner.png">

---

### Multiplayer Ready!
### Last Updated - 11/09/2020 | Latest Version Tested - 12.01 | Current Mod Version - 1.0.1

---

## Summary

This simple utility mod allows you to quickly transfer the same type of item by holding either the left or right `Ctrl + Shift` and clicking the item between inventories. The function is replicated to work similar to popular games such as Minecraft that have this convenience functionality built-in.
