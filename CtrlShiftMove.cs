﻿using System.Reflection;
using System.Collections.Generic;
using HarmonyLib;
using UnityEngine;
using UnityEngine.EventSystems;

public class CtrlShiftMove : Mod {
  private Harmony harmony;
  private string instanceId = "com.bahamut.ctrlshiftmove";

  public void Start() {
    harmony = new Harmony(instanceId);
    harmony.PatchAll(Assembly.GetExecutingAssembly());
    Debug.Log("Mod CtrlShiftMove has been loaded!");
  }

  public void OnModUnload() {
    harmony.UnpatchAll(instanceId);
    Debug.Log("Mod CtrlShiftMove has been unloaded!");
  }

  // Patches
  [HarmonyPatch(typeof(Slot), "OnPointerDown")]
  class SlotOnPointerDown_Patch {
    private static void Prefix(Slot __instance, PointerEventData eventData, out Item_Base __state) {
      if (__instance.itemInstance == null) {
        __state = null;
      } else if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))) {
        __state = __instance.itemInstance.baseItem;
      } else {
        __state = null;
      }
    }

    private static void Postfix(Slot __instance, PointerEventData eventData, Item_Base __state) {
      if (__state != null) {
        if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))) {
          PlayerInventory playerInventory = RAPI.GetLocalPlayer().Inventory;
          Inventory instanceInventory = Traverse.Create(__instance).Field("inventory").GetValue() as Inventory;
          string itemName = __state.UniqueName;
          List<Slot> targetSlots = (instanceInventory == playerInventory) ? playerInventory.allSlots : instanceInventory.allSlots;
          foreach (Slot slot in targetSlots) {
            if (slot.itemInstance != null && slot.itemInstance.baseItem != null && slot.itemInstance.baseItem.UniqueName.Equals(itemName)) {
              instanceInventory.ShiftMoveItem(slot, eventData);
            }
          }
        }
      }
    }
  }
}
