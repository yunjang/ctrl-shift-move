# 1.1.0
- Right `Ctrl + Shift` Keys are now supported to be more accessible to left-handed folks

# 1.0.1
- Modinfo now reflects correct versioning

# 1.0.0
- Initial Release